var pry = require('pryjs');
var ObjectID = require('mongodb').ObjectID;

module.exports = function(app, db) {
  // GET list of notes
  app.get('/notes', (req, res) => {
    db.collection('notes').find().toArray((err, notes) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(notes);
      }
    });
  });

  // GET a note
  app.get('/notes/:id', (req, res) => {
    var details = { '_id': new ObjectID(req.params.id) };
    db.collection('notes').findOne(details, (err, item) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(item);
      }
    });
  });

  // POST a new note
  app.post('/notes', (req, res) => {
    eval(pry.it);
    var note = { text: req.body.body, title: req.body.title };
    db.collection('notes').insert(note, (err, result) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(result.ops[0]);
      }
    });
  });

  // UPDATE a note
  app.put('/notes/:id', (req, res) => {
    var details = { '_id': new ObjectID(req.params.id) };
    var note = { $set: req.body };
    db.collection('notes').update(details, note, (err, result) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(result);
      }
    });
  });

  // DELETE a note
  app.delete('/notes/:id', (req, res) => {
    var details = { '_id': new ObjectID(req.params.id) };
    db.collection('notes').remove(details, (err, item) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send('Note ' + req.params.id + ' deleted!');
      }
    });
  });
};
