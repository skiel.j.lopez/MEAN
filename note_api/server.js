const express = require('express');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const db = require('./config/db');

const app = express();
const port = 8080;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

MongoClient.connect(db.url, (err, result) => {
  if (err) return console.log(err);

  require('./app/routes')(app, result.db('node_notes'));

  app.listen(port, () => {
    console.log('App listening on http://localhost:' + port);
  });
})
