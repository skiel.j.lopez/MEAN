import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  public posts: Observable<any[]>;

  constructor(private _blogSrvc: BlogService) { }

  ngOnInit() {
    this.getPosts();
  }

  getPosts() {
    this.posts = this._blogSrvc.getPostsIndex();
  }
}
