import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class BlogService {

  constructor(
    private _http: HttpClient,
  ) { }

  getPostsIndex(): Observable<any[]> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    return this._http.get<any[]>('posts');
  }
}
