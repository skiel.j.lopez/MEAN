import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpEvent
       , HttpInterceptor
       , HttpHandler
       , HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newReq = req.clone({ url: environment.apiBaseUrl + req.url });
    return next.handle(newReq);
  }

}
