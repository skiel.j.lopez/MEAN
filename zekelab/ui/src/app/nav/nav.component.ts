import { Component, OnInit } from '@angular/core';
import { trigger
       , state
       , style
       , animate
       , transition
       , query
       , group
       , animateChild } from '@angular/animations';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
  animations: [
    trigger('navAnim', [
      state('home', style({
        top: '50%',
        left: '50%',
        width: '80vmin',
        height: '80vmin',
        marginTop: '-40vmin',
        marginLeft: '-40vmin',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
      })),
      state('away', style({
        top: '50%',
        left: '10px',
        height: '80vmin',
        width: '40px',
        marginTop: '-40vmin',
        marginLeft: 0,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
      })),
      transition('home => away', [
        group([
          query('@circleAnim', [
            animateChild()
          ]),
          animate('1.5s ease-in-out')
        ])
      ]),
      transition('away => home', [
        group([
          query('@circleAnim', [
            animateChild()
          ]),
          animate('1.5s ease-in-out')
        ])
      ])
    ]),
    trigger('circleAnim', [
      state('home', style({
        position: 'absolute',
        margin: '10px 0',
        transform: 'rotateZ(0deg)'
      })),
      state('away', style({
        position: 'relative',
        margin: '10px 0',
        transform: 'rotateZ(-90deg)'
      })),
      transition('home => away', [
        animate('1.5s ease-in-out')
      ]),
      transition('away => home', [
        animate('1.5s ease-in-out')
      ])
    ])
  ]
})
export class NavComponent implements OnInit {

  atHome: boolean = true; 

  constructor() { }

  ngOnInit() { }

  setAnimation(inHome: boolean) {
    this.atHome = inHome;
  }
}
