import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <router-outlet></router-outlet>
  <app-nav></app-nav>
  `,
  styles: []
})
export class AppComponent {
}
