var mongoose = require('mongoose');
var pry = require('pryjs');
require('../models/BlogPost');

var BlogPost = mongoose.model('BlogPost');

module.exports = function(app) {
  app.route('/api/post/:postId/comments')
    .get((req, res, next) => {
      BlogPost.find({ deletedAt: null }, (err, posts) => {
        err ? next(err) : res.json(posts);
      });
    })
    .post((req, res, next) => {
      let newPost = new BlogPost(req.body);
      newPost.save((err, post) => {
        if (err) next(err);
        res.json(post);
      });
    });

  app.route('/api/post/:postId/comments/:id')
    .get((req, res, next) => {
      BlogPost.findById(req.params.id, (err, post) => {
        if (err) next(err);

        res.json(post);
      });
    })
    .put((req, res, next) => {
      BlogPost.findByIdAndUpdate(
        req.params.id,
        postParams(req.body),
        (err, post) => {
          if (err) next(err);
          res.json(true);
      });
    })
    .delete((req, res, next) => {
      BlogPost.findByIdAndUpdate(
        req.params.id,
        { deletedAt: Date.now() },
        (err, post) => {
          if (err) next(err);
          res.json(true);
        }
      );
    });
}

function getPost(postId) {
  BlogPost.FindById(postId, (err, post) => {
    err ? next(err) : return post;
  });
}

function postParams(body) {
  let validParams = {};
  if (body.title) validParams.title = body.title;
  if (body.body) validParams.body = body.body;
  return validParams;
}

