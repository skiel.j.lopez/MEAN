var mongoose = require('mongoose');
require('../models/BlogPost');

var BlogPost = mongoose.model('BlogPost');

module.exports = function(app) {
  app.route('/api/posts')
    .get((req, res, next) => {
      BlogPost.find({ deletedAt: null }, (err, posts) => {
        err ? next(err) : res.json(posts);
      });
    })
    .post((req, res, next) => {
      let newPost = new BlogPost(req.body);
      newPost.save((err, post) => {
        err ? next(err) : res.json(post);
      });
    });

  app.route('/api/post/:id')
    .get((req, res, next) => {
      BlogPost.findById(req.params.id, (err, post) => {
        err ? next(err) : res.json(post);
      });
    })
    .put((req, res, next) => {
      BlogPost.findById(req.params.id, (err, post) => {
          if (err) {
            next(err);
          } else {
            if (req.body.comments && req.body.comments.user && req.body.comments.body) {
              post.comments = [...post.comments, req.body.comments];
            }
            if (req.body.title) post.title = req.body.title;
            if (req.body.body) post.body = req.body.body;

            post.save((err, post) => {
              err ? next(err) : res.json(true);
            });
          }
        }
      );
    })
    .delete((req, res, next) => {
      BlogPost.findByIdAndUpdate(
        req.params.id,
        { deletedAt: Date.now() },
        (err, post) => {
          err ? next(err) : res.json(true);
        }
      );
    });
}
