#! /bin/bash

docker run -d -p 27017:27017 \
       --name zekelab-mongo \
       -v "$( pwd )/data":/data/db \
       -t mongo:latest && sleep 2 && npm start
