const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const db = require('./config/db');

const app = express();
const port = 5700;

// Mongo configuration
mongoose.Promise = global.Promise;
mongoose.connect(db.url, { useNewUrlParser: true });

// App configuration
app.options('localhost:8080', cors());
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ 'extended': 'true' }));
app.use(bodyParser.json());
app.use(bodyParser.json({ 'type': 'application/vnd.api+json' }));

require('./routes')(app);
require('./error_handler')(app);

app.listen(port);

console.log('API running on port:', port);
