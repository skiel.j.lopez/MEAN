const ObjectID = require('mongodb').ObjectID;

module.exports = function(app, db) {
  // Index
  app.get('/todos', (req, res) => {
    db.collection('todos').find().toArray((err, todos) => {
      if (err) {
        res.send({
          'code': err.code,
          'error': err.message
        });
      } else {
        res.send(todos);
      }
    });
  });

  // Show
  app.get('/todos/:id', (req, res) => {
    var q = { '_id': new ObjectID(req.params.id) };
    db.collection('todos').findOne(q, (err, todo) => {
      if (err) {
        res.send({
          'code': err.code,
          'error': err.message
        });
      } else {
        res.send(todo);
      }
    });
  });

  // Create
  app.post('/todos', (req, res) => {
    var todo = req.body;
    db.collection('todos').insert(todo, (err, result) => {
      if (err) {
        res.send({
          'code': err.code,
          'error': err.message
        });
      } else {
        res.send(result);
      }
    });
  });

  // Update
  app.put('/todos/:id', (req, res) => {
    var q = { '_id': new ObjectID(req.params.id) };
    var todo = { $set: req.body }; 
    db.collection('todos').update(q, todo, (err, result) => {
      if (err) {
        res.send({
          'code': err.code,
          'error': err.message
        });
      } else {
        res.send(result);
      }
    });
  });

  // Delete
  app.delete('/todos/:id', (req, res) => {
    var q = { '_id': new ObjectID(req.params.id) };
    db.collection('todos').remove(q, (err, result) => {
      if (err) {
        res.send({
          'code': err.code,
          'error': err.message
        });
      } else {
        res.send(result);
      }
    });
  });
};
