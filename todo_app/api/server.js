// IMPORTS
const express     = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser  = require('body-parser');
const morgan      = require('morgan');

const db = require('./config/db');

const app  = express();
const port = 8080;

// CONFIGS
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ 'extended': 'true' }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

MongoClient.connect(db.url, (err, conn) => {
  if (err) return console.log(err);

  require('./routes')(app, conn.db('todos'));

  app.listen(port, () => {
    console.log('App listening on http://localhost:' + port);
  });
});
