#! /bin/bash

# run mongo for this applicatio

docker run -d --name todo-mongo \
           -v "$HOME/projects/MEAN/todo_app/db:/data/db" \
           -p 27017:27017 -t mongo:latest
