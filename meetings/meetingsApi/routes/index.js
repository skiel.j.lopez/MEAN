const blogRoutes = require('./blog');

module.exports = function(app) {
  blogRoutes(app);
}
