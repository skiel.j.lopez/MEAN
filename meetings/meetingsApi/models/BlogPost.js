var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
  user: String,
  body: String,
  createdAt: { type: Date, default: Date.now }
});

var BlogPostSchema = new Schema({
  title: String,
  body: String,
  comments: [CommentSchema],
  createdAt: { type: Date, default: Date.now },
  deletedAt: { type: Date, default: null }
});

module.exports = mongoose.model('BlogPost', BlogPostSchema);
