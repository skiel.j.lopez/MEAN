const express = require('express');
const cors = require('cors');
const cookies = require('cookie-parser');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const passport = require('passport');
// read in configuration from .env file
require('dotenv').config();

const app = express();
// set basic config constants
const port = config.env.PORT;
const dbPort = config.env.DB_PORT;
const dbHost = config.env.DB_URL;
const dbCollection = config.env.DB_COLLECTION;
const dbUrl = `mongodb://${dbHost}:${dbPort}/${dbCollection}`;

// Mongo configuration
mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise;
mongoose.connect(dbUrl, { useNewUrlParser: true });

// Passport configuration
app.use(passport.initialize());
require('./passport-config')(passport);

// App configuration
app.options(`${config.env.UI_HOST}:${config.env.UI_PORT}`, cors());
app.use(cors());
app.use(morgan('dev'));
app.use(cookies());
app.use(bodyParser.urlencoded({ 'extended': 'true' }));
app.use(bodyParser.json());
app.use(bodyParser.json({ 'type': 'application/vnd.api+json' }));

require('./routes')(app);
require('./error_handler')(app);

app.listen(port);

console.log('API running on port:', port);
