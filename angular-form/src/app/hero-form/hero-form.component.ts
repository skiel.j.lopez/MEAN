import { Component, OnInit } from '@angular/core';

import { Hero } from '../classes/hero';

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.css']
})
export class HeroFormComponent implements OnInit {
  powers = [
    'Really Smart',
    'Fly',
    'Magic',
    'Really Strong'
  ];

  model = new Hero(
    1,
    'Anorak',
    this.powers[2],
    'Ezequiel Lopez'
  );

  submitted = false;

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
  }

  get diagnostic() {
    return JSON.stringify(this.model);
  }
}
