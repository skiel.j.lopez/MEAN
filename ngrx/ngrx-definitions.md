What is Application State?
--------------------------

- Server response data
- User information
- User input
- UI State
- Router / location state

The state lives inside the store. The store is a container to have in one
location all the states of our application
We compose application state in our Store.
The state is a representation of the data of our application.

State Management Libraries. (ngrx-store | ngrx-state)
-----------------------------------------------------

- Model our app state
- Update state
- Read state values
- Monito / Observe changes to state

Redux 3 Principles
------------------

1. Single sourece of truth

  - One state tree inside Store
  - Predictability, maintainability
  - Universal apps (SSR)
  - Testing and debugging

2. State is read-only

  - Derive properties from state
  - Dispatch actions to change the state
  - Immutable update patterns

3. Pure functions update state

  - Pure functions are reducers
  - Reducers respond to action types
  - Reducers return new state

Redux core concepts
-------------------

- Single state tree
- Actions
- Reducers
- Store
- One-way dataflow

Single state tree
-----------------

- Plain JavaScript Object
- Composed by reducers

  e.g.
  `const state = { todos:[] };`

Actions
-------

- Two properties
  - type: string, describes event
  - payload: optional data
- Dispatch actions to reducers

  e.g.
```
  const action = {
      type: 'ADD_TODO',
      pyload: {
          label: 'Eat pizza',
          complete: false
      }
  }
```

Reducers
--------

- Pure functions
- Given dispatched action
  - Responds to action.type
  - Access to action.payload
  - Composes new state
  - Returns new state

  e.g.
```
  function reducer(state, action) {
      switch(action.type) {
          case 'ADD_TODO': {
              const todo = action.payload;
              const todos = [...state.todos, todo];
              return { todos };
          }
      }
      return state;
  }
```

Store
-----

- State container
- Components interact with the Store
  - Subscribe to slices of State
  - Dispathc Actions to the Store
- Store invokes Reducers with previous State and Action
- Reducers compose new State
- Store is update, notifies subscribers

One-way dataflow
----------------

```
                      +============================+
                      |                            |
    |-------------------------> Action ------|     |
    |                 |                      |     |
    | dispatch        |              Sent to |     |
    |                 |                      |     |
  -------------       |                      v     |
  | Component |       |   STORE           Reducer  |
  -------------       |                      |     |
    ^                 |                      |     |
    | Render          |            New state |     |
    |                 |                      |     |
    |-------------------------- State <------|     |
                      |                            |
                      +============================+
```

Mutable and Immutable operations
--------------------------------

- Immutability definition

  An immutable object is an object whose state cannot be modified after creation.

- Why immutable?

  - Predictability
  - Explicit state changes
  - Performance (Change Detection)
  - Mutation Tracking
  - Undo state changes

- Mutability in JavaScript

  - Functions
  - Objects
  - Arrays

  The objects avobe are consider Mutable because you can add and remove
  properties or values from them modifying the main object.

- Immutability in JavaScript

  - Strings
  - Numbers

  Strings and Numbers are Immutables because the functions to modify them
  always return a new copy of the objects with the modified value

What is ngrx/store?
-------------------

Redux inspired reactive state management (Observable driven state management)

- Based on Redux
- Written with Observables
- Made for Angular

Benefits of ngrx/store
----------------------

- Single sourece of truth
- Testability
- Performance Benefits
  - ChangeDetectionStrategy.OnPush
  - Immutable @Inputs
  - Object reference checks are fast
- Root and feature module support
  - Eagerly loaded modules
  - Lazily loaded modules

Reactive Angular architecture
-----------------------------

```
--------------------------   --------------------------------
|  Container Component   |   |  Presentational Component    |
--------------------------   --------------------------------
 - Aware of Store             - Not aware of Store
 - Dispatches Actions         - Invokes callbacks via @Output
 - Reads data from Store      - Reads data from @Inputs
```

```
                         ---------
                         | Store |
                         ---------
                 Dispatch  ^   |  Select
                           |   v
                  -----------------------
                  | Container Component |
                  -----------------------
          @Output  ^   |           ^   |  @Input
                   |   v           |   v
----------------------------   ----------------------------
| Presentational Component |   | Presentational Component |
----------------------------   ----------------------------
```
