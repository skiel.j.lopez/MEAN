var canvas = document.querySelector('#my-canvas');
var canvasContainer = document.querySelector('.canvas-container');

canvas.width = canvasContainer.offsetWidth;
canvas.height = canvasContainer.offsetHeight;

var ctx = canvas.getContext('2d');
var style = getComputedStyle(document.documentElement);

var leftPressed = false; 
var rightPressed = false;

document.addEventListener('keydown', keyDownHandler, false);
document.addEventListener('keyup', keyUpHandler, false);

function keyDownHandler(e) {
  if (e.keyCode == 39) rightPressed = true;
  if (e.keyCode == 37) leftPressed = true;
}

function keyUpHandler(e) {
  if (e.keyCode == 39) rightPressed = false;
  if (e.keyCode == 37) leftPressed = false;
}

class Ball {
  constructor() {
    this.radius = 10;
    this.posX = (canvas.width - this.radius) / 2;
    this.posY = canvas.height - 50;
    this.desX = -2;
    this.desY = -2;
  }

  bounceTop() {
    if (this.posY - this.radius < 0) this.desY = -this.desY;
  }

  bounceSides() {
    if (this.posX - this.radius < 0
     || this.posX + this.radius > canvas.width) this.desX = -this.desX;
  }

  bouncePaddle() {
    if (this.posY + this.radius == paddle.posY
        && this.posX > paddle.posX
        && this.posX < paddle.posX + paddle.width) this.desY = -this.desY;
  }

  gameOver() {
    if (this.posY + this.radius > canvas.height) {
      clearInterval(animation);
      alert('GAME OVER');
      document.location.reload();
    }
  }

  move() {
    // bounce of the wall
    this.bounceTop();
    this.bounceSides();
    // collision with paddle
    this.bouncePaddle();

    this.posX += this.desX;
    this.posY += this.desY;
  }

  draw() {
    ctx.beginPath();
    ctx.arc(this.posX, this.posY, this.radius, 0, Math.PI * 2);
    ctx.fillStyle = style.getPropertyValue('--main-color');
    ctx.fill();
    ctx.closePath();
  }
};

class Paddle {
  constructor() {
    this.height = 15;
    this.width = 90;
    this.posX = (canvas.width - this.width) / 2;
    this.posY = canvas.height - 20;
  }

  move() {
    if (leftPressed && this.posX > 0) this.posX -= 7;
    if (rightPressed && this.posX + this.width < canvas.width)
      this.posX += 7;
  }

  draw() {
    ctx.beginPath();
    ctx.rect(this.posX, this.posY, this.width, this.height);
    ctx.fillStyle = style.getPropertyValue('--main-color');
    ctx.fill();
    ctx.closePath();
  }
};

class Brick {
  constructor(x, y) {
    this.width = 75;
    this.height = 20;
    this.posX = x;
    this.posY = y;
  }

  draw() {
    ctx.beginPath();
    ctx.rect(this.posX, this.posY, this.width, this.height);
    ctx.fillStyle = style.getPropertyValue('--main-color');
    ctx.fill();
    ctx.closePath();
  }
}

class Wall {
  constructor(rows, columns) {
    this.rows = rows;
    this.columns = columns;
    this.initX = (canvas.width - columns * (75 + 5)) / 2;
    this.initY = 30;
    this.padding = 5;
  }

  draw() {
    let brickWidth;
    let brickHeight;
    let posY = this.initY;
    for (let r=1; r<=this.rows; r++) {
      let posX = this.initX;
      for (let c=1; c<=this.columns; c++) {
        let brick = new Brick(posX, posY);
        if (!brickWidth) brickWidth = brick.width;
        if (!brickHeight) brickHeight = brick.height;
        posX += brickWidth + this.padding;
        brick.draw();
      }
      posY += brickHeight + this.padding;
    }
  }
}

var wall = new Wall(4, Math.floor((canvas.width - 150) / 75));
var ball = new Ball();
var paddle = new Paddle();

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  wall.draw();

  paddle.move();
  paddle.draw();

  ball.move();
  ball.draw();
  ball.gameOver();
}
animation = setInterval(draw, 10);
