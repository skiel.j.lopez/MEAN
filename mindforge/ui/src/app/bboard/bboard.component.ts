import { Component, OnInit } from '@angular/core';
import { BboardService } from './bboard.service';

@Component({
  selector: 'app-bboard',
  templateUrl: './bboard.component.html',
  styleUrls: ['./bboard.component.scss']
})
export class BboardComponent implements OnInit {
  public data: any[];

  constructor(public bboardSrvc: BboardService) { }

  ngOnInit(): void {
    this.bboardSrvc.getBulletins().subscribe(resp => {
      this.data = resp;
    });
  }

  addTag(bulletin: any) {
    this.bboardSrvc.addTag(bulletin.id, bulletin.newTag).subscribe(resp => {
      bulletin.tags.push(bulletin.newTag);
    });
  }
}
