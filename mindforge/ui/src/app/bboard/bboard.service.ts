import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BboardService {

  constructor(private _http: HttpClient) { }

  getBulletins(): Observable<any[]> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    return this._http.get<any[]>('/bulletins', options);
  }

  addTag(bulletinId: number, tag: string): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    return this._http.post<any>(`/bulletin/${bulletinId}/tags`, [tag], options);
  }
}
