import { Injectable } from '@angular/core';
import { HttpEvent
       , HttpInterceptor
       , HttpHandler
       , HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  private _APIBaseURL: string = 'http://100.115.92.199:5700/api/v1';

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newReq = req.clone({ url: this._APIBaseURL + req.url });
    return next.handle(newReq);
  }
}
