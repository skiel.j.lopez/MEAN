import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BboardComponent } from './bboard/bboard.component';


const routes: Routes = [
  { path: '', component: BboardComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
