const checkBulletin = (req, res, next) => {
  let bulletin = req.body;
  if(!bulletin || !bulletin.title || !bulletin.body || !bulletin.created_by) {
    let err = new Error('Invalid bulletin');
    err.statusCode = 400;
    throw err;
  }
  next();
};

const checkTags = (req, res, next) => {
  let tags = req.body;
  if(!tags || tags.length == 0) {
    let err = new Error('Invalid bulletin');
    err.statusCode = 400;
    throw err;
  }
  next();
};

module.exports = {
  checkBulletin,
  checkTags,
};
