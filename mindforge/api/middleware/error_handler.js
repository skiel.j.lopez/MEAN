/* Global Error Handler */
module.exports = function(app) {
  // middleware
  app.use((error, req, res, next) => {
    if (!error.statusCode) error.statusCode = 500;
    console.log(error);
    res.status(error.statusCode).json({
      code: error.statusCode,
      message: error.message,
    });
  });

  // capture all rough requests
  app.get('*', (req, res, next) => {
    let error = new Error('Not Found');
    error.statusCode = 404;
    res.status(error.statusCode).json({
      code: error.statusCode,
      message: error.message
    });
  });
}
