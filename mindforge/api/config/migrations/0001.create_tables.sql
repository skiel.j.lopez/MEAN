-- Run multiple files
-- cat 0001.create_tables.sql 0002.create_tags.sql | psql -d mfdb -U mfuser -h localhost

BEGIN;
    DROP TABLE IF EXISTS bulletins;

    CREATE TABLE bulletins (
        id SERIAL PRIMARY KEY,
        title VARCHAR(100) NOT NULL,
        body TEXT NOT NULL,
        created_by VARCHAR(100) NOT NULL,

        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        deleted_at TIMESTAMP 
    );
END;

BEGIN;
    INSERT INTO bulletins
    (title, body, created_by)
    VALUES
    ('1 test', 'Firts bulletin posted', 'ezequiel.lopez');
END;
