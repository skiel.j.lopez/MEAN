BEGIN;
    DROP TABLE IF EXISTS tags;

    CREATE TABLE tags (
        id SERIAL PRIMARY KEY,
        title VARCHAR(100) NOT NULL,
        bulletin_id INTEGER REFERENCES bulletins,

        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );
END;

BEGIN;
    INSERT INTO tags (title, bulletin_id) VALUES ('test', 1);
END;
