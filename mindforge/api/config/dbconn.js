// Create DB with psql

// connect to postgres db with postgres user
// $ sudo -u postgres psql => 

// Create DB
// postgres=# create database mfdb;

// Create user for DB
// postgres=# create user mfuser with encrypted password 'mfpassword';

// Grant all privileges to mfuser on mfdb
// postgres=# grant all privileges on database mfdb to mfuser;

const Pool = require('pg').Pool;
const pool = new Pool({
  user: 'mfuser',
  password: 'mfpassword',
  host: 'localhost',
  database: 'mfdb',
  port: 5432
});

module.exports = pool; 
