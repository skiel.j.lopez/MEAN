const pool = require('../../config/dbconn');

const getBulletins = cb => {
  let sqlSelect = `
    SELECT b.*, t.title as tag
      FROM bulletins b
      LEFT JOIN tags t ON t.bulletin_id = b.id
     WHERE b.deleted_at IS NULL;`;

  pool.query(sqlSelect, (err, result) => { 
    let bulletins = [];

    result.rows.forEach(row => {
      let bulletin = bulletins.find(b => b.id == row.id);
      if(bulletin) {
        bulletin.tags.push(row.tag);
      } else {
        bulletins.push({
          id: row.id,
          title: row.title,
          body: row.body,
          tags: row.tag ? [row.tag] : [],
          created_by: row.created_by,
          created_at: row.created_at,
          updated_at: row.updated_at
        });
      }
    });

    cb(err, bulletins);
  });
};

const getBulletinById = (id, cb) => {
  let sqlSelect = `
    SELECT b.*, t.title as tag
      FROM bulletins b
      LEFT JOIN tags t ON t.bulletin_id = b.id
     WHERE b.id = $1;`;

  pool.query(sqlSelect, [id], (err, result) => { 
    let bulletin;

    result.rows.forEach(row => {
      if(bulletin) {
        bulletin.tags.push(row.tag);
      } else {
        bulletin = {
          id: row.id,
          title: row.title,
          body: row.body,
          tags: row.tag ? [row.tag] : [],
          created_by: row.created_by,
          created_at: row.created_at,
          updated_at: row.updated_at
        };
      }
    });

    cb(err, bulletin);
  });
};

const newBulletin = (bulletin, cb) => {
  let sqlInsert = `
    INSERT INTO bulletins
    (title, body, created_by)
    VALUES
    ($1, $2, $3)
    RETURNING id;`;

  pool.query(
    sqlInsert,
    [bulletin.title, bulletin.body, bulletin.created_by],
    (err, result) => { cb(err, result); }
  );
};

const addTags = (bulletinId, tags, cb) => {
  let sqlInsert = `
    INSERT INTO tags
    (bulletin_id, title)
    SELECT $1, UNNEST($2::text[])
    RETURNING id`;

  let results = [];
  pool.query(
    sqlInsert,
    [bulletinId, tags],
    (err, result) => { cb(err, result); }
  );
}

module.exports = {
  getBulletins,
  getBulletinById,
  newBulletin,
  addTags,
};
