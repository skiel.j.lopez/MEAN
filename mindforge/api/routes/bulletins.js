const bulletinQ = require('../modules/bulletinboard/boardSQLs.js');
const { checkBulletin, checkTags } = require('../middleware/reqVals');

module.exports = function(app) {
  app.route('/api/v1/bulletins').get(
    (req, res, next) => {
      bulletinQ.getBulletins((err, bulletins) => {
        if(err) next(err);
        res.json(bulletins);
      });
    }
  );

  app.route('/api/v1/bulletin/:id').get(
    (req, res, next) => {
      bulletinQ.getBulletinById(req.params.id, (err, bulletin) => {
        if(err) next(err);
        res.json(bulletin);
      });
    }
  );

  app.route('/api/v1/bulletin').post(
    checkBulletin,
    (req, res, next) => {
      let bulletin = req.body;
      bulletinQ.newBulletin(bulletin, (err, resp) => {
        if(err) next(err);
        if(bulletin.tags) {
          bulletinQ.addTags(resp.rows[0].id, bulletin.tags, (err, resp) => {
            if(err) next(err);
            res.json(resp);
          });
        } else {
          res.json(resp);
        }
      });
    }
  );

  app.route('/api/v1/bulletin/:id/tags').post(
    checkTags,
    (req, res, next) => {
      let tags = req.body;
      bulletinQ.addTags(req.params.id, tags, (err, resp) => {
        if(err) next(err);
        res.json(resp);
      });
    }
  );
}
