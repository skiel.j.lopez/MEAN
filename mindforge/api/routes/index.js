const bulletinRoutes = require('./bulletins');

module.exports = function(app) {
  bulletinRoutes(app);
}
