const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const app = express();
const port = 5700;

app.options('localhost:4200', cors());
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ 'extended': 'true' }));
app.use(bodyParser.json());
app.use(bodyParser.json({ 'type': 'application/vnd.api+json' }));

require('./routes')(app);
require('./middleware/error_handler')(app);

app.listen(port);
console.log('API running on port:', port);
