var passport = require('passport');

module.exports = function(app) {
  app.route('/test').get(
    passport.authenticate('jwt', { session: false }),
    (req, res, next) => {
      res.json({
        method: req.method,
        ip: req.ip,
        url: req.url,
      });
    }
  );
}
