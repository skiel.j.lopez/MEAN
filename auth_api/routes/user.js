var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
// Bcryptjs is a no setup encryption tool.

require('../models/user');
var User = mongoose.model('User');

require('dotenv').config();
const secret = process.env.SECRET || 'Ch33tosButt1@';
const passport = require('passport');
const jwt = require('jsonwebtoken');

module.exports = function(app) {
  app.route('/register').post((req, res, next) => {
    if(!req.body.username || req.body.username.replace(/\s+/g, '') == '')
      return res.status(400).json('Username is required');
    if(!req.body.email || req.body.email.replace(/\s+/g, '') == '')
      return res.status(400).json('Email is required');
    if(!req.body.password || req.body.password.replace(/\s+/g, '') == '')
      return res.status(400).json('Password is required');
    User.findOne({ username: req.body.username }).then(user => {
      if(user) {
        let error = 'User already exists in database';
        return res.status(400).json(error);
      } else {
        let newUser = new User({
          username: req.body.username,
          email: req.body.email,
          password: req.body.password
        });
        bcrypt.genSalt(10, (err, salt) => {
          if(err) throw err;
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if(err) throw err;
            newUser.password = hash;
            newUser.save().then(user => res.json(user))
                          .catch(err => res.status(400).json(err));
          });
        });
      }
    });
  });

  app.route('/login').post((req, res, next) => {
    let username = req.body.username;
    let password = req.body.password;
    User.findOne({ username: username }).then(user => {
      if(!user) return res.status(404).json('Invalid user');
      bcrypt.compare(password, user.password).then(isMatch => {
        if(isMatch) {
          let payload = { id: user._id, username: user.username };
          jwt.sign(payload, secret, { expiresIn: 36000 }, (err, token) => {
            if(err) res.status(500).json({
              error: 'Error signing token',
              raw: err
            });
            return res.json({ success: true, token: `Bearer ${token}` });
          });
        } else {
          return res.status(400).json('Invalid password');
        }
      });
    });
  });
}
