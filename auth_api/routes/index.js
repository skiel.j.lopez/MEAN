const userRoutes = require('./user');
const testRoute = require('./home');

module.exports = function(app) {
  userRoutes(app);
  testRoute(app);
}
