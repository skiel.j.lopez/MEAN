const { Strategy, ExtractJwt } = require('passport-jwt');
// this is using ES6 destructuring. If you are not using a build step, it could
// cause issues and is equivalent to
//const pp-jwt = require('passport-jwt');
//const Strategy = pp-jwt.Stratety;
//const ExtractJwt = pp-jwt.ExtractJwt;

require('dotenv').config();

const secret = process.env.SECRET || 'Ch33tosButt1@';
const mongoose = require('mongoose');
const User = require('./models/user');

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: secret
};

// this sets how we handle tokens coming from the request that come and also
// defines the key to be used when verifying the token.

module.exports = passport => {
  passport.use(
    new Strategy(opts, (payload, done) => {
      User.findById(payload.id)
          .then(user => {
            if(!user) return done(null, false);
            return done(null, {
              id: user.id,
              name: user.name,
              email: user.email
            });
          })
          .catch(err => console.error(err));
    })
  );
};
