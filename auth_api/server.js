const express = require('express');
const cookies = require('cookie-parser');
const bp = require('body-parser');

require('dotenv').config();
// reads in configuration from a .env file

const mongoose = require('mongoose');
const passport = require('passport');

const app = express();

const port = process.env.PORT || 3000;
const dbPort = process.env.DB_PORT || 27017;
const dbUrl = process.env.DB_URL || 'localhost';
const dbCollection = process.env.DB_COLLECTION || 'auth_test';
// sets the required variables from Environment Variables.

mongoose.set('useCreateIndex', true);
// fixes an issue with a depricated default in Mongoose js
mongoose.connect(`mongodb://${dbUrl}/${dbCollection}`, { useNewUrlParser: true })
        .then(_ => console.log('Connected Successfully to MongoDB'))
        .catch(err => console.error(err));

app.use(passport.initialize());
// initializes the passport configuration.

require('./passport-config')(passport);
// imports our configuration file which holds our verification callbacks and
// things like the secrete for signing.

app.use(cookies());
app.use(bp.urlencoded({ extended: false }));
app.use(bp.json());

app.use((req, res, next) => {
  console.info(`[ ${req.method} ] ${req.ip} => ${req.url}`);
  next();
});

// custom middleware to log every incoming request.
require('./routes')(app);
// registers our authentication routes with express.

app.listen(port, err => {
  err ? console.error(err) : console.log(`API listening on port: ${port}`);
});
