import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

import { Hero, HEROES } from '../classes/data-model';

@Injectable()
export class HeroService {
  delayMs = 500;

  constructor() { }

  getHeroes(): Observable<Hero[]> {
    return of(HEROES).delay(this.delayMs);
  }
}
