import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormArray, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Address, Hero, states } from '../classes/data-model'

//@Component({
//  selector: 'app-hero-detail',
//  templateUrl: './hero-detail.component.html',
//  styleUrls: ['./hero-detail.component.scss']
//})
//export class HeroDetailComponent implements OnInit {
//  heroForm = new FormGroup({
//    name: new FormControl()
//  });
//
//  constructor() { }
//
//  ngOnInit() {
//  }
//
//}

// FormBuilder
// ===========
//
//@Component({
//  selector: 'app-hero-detail',
//  templateUrl: './hero-detail.component.html',
//  styleUrls: ['./hero-detail.component.scss']
//})
//export class HeroDetailComponent implements OnInit {
//  heroForm: FormGroup;
//  states = states;
//
//  constructor(private fb: FormBuilder) {
//    this.createForm();
//  }
//
//  createForm() {
//    this.heroForm = this.fb.group({
//      name: ['', Validators.required ],
//      address: this.fb.group(new Address()),
//      power: '',
//      sidekick: false,
//    });
//  }
//
//  ngOnInit() {
//  }
//}

// setValue patchValue
// ===================
// Helper functions to build the form controllers using the classes definition.
// setValue checks for validations, if the formGroup doesn't match the class
// definition it rise and error. The patchValue falis silentlly.
@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit, OnChanges {
  heroForm: FormGroup; // <-- simple initialization specifying the data type
  states = states;
  @Input() hero: Hero; // <-- to be able to assing a value from a different component

  constructor(
    private fb: FormBuilder
  ) {
    // Create the form in the constructor using javascript.
    // OnChanges executes before than OnInit.
    this.createForm();
  }

  createForm() {
    this.heroForm = this.fb.group({
      name: '',
      address: this.fb.array([]),
      power: '',
      sidekick: false,
    })
  }

  // We assign the data from the hero when the user change something on the form
  // instead of using setValue we can use 'reset' that sets the values of the
  // model and resets the form status to prestine and so
  ngOnInit() {
  }

  ngOnChanges() {
    this.heroForm.reset({
      name: this.hero.name,
      //address: this.hero.addresses[0] || new Address(),
      //secretLairs: this.setAddresses(this.hero.addresses), //this.fb.array([]),
      power: '',
      sidekick: false,
    });
    this.setAddresses(this.hero.addresses);
  }

  setAddresses(addresses: Address[]) {
    const addressFGs = addresses.map(address => this.fb.group(address));
    const addressFormArray = this.fb.array(addressFGs);
    this.heroForm.setControl('secretLairs', addressFormArray);
  }

  get secretLairs(): FormArray {
    return this.heroForm.get('secretLairs') as FormArray;
  }
}
