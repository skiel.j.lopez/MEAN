export class Hero {
  id = 0;
  name = '';
  addresses: Address[];
}

export class Address {
  street = '';
  city = '';
  state = '';
  zip = '';
}

export const HEROES: Hero[] = [
  {
    id: 1,
    name: 'Whirlwind',
    addresses: [
      { street: '123 Elm st', city: 'Granville', state: 'OH', zip: '43023' },
      { street: '456 Strathmore Lane', city: 'Dublin', state: 'OH', zip: '43017' }
    ]
  },
  {
    id: 2,
    name: 'Bombastic',
    addresses: [
      { street: '789 Elm st', city: 'Jacksonville', state: 'OH', zip: '43073' }
    ]
  },
  {
    id: 3,
    name: 'Magneta',
    addresses: [ ]
  }
]

export const states = [ 'CA', 'MD', 'OH', 'VA' ]
